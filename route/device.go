package route

import (
	"fmt"
	"more_ssh/conf"
	"more_ssh/sql"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ResultHost struct {
	sql.Host
	Accounts []sql.Account `json:"accounts"`
}

func list(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "0"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "请求的参数有误，请检查 page 参数是否是整数",
		})
		return
	}
	pagesize, err := strconv.Atoi(c.DefaultQuery("pagesize", "10"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "请求的参数有误，请检查 pagesize 参数是否是整数",
		})
		return
	}

	switch {
	case pagesize > 1000:
		pagesize = 1000
	case pagesize <= 10:
		pagesize = 10
	}

	// pagedb := sql.Paginate(page, pagesize)
	var hosts []sql.Host
	// conf.DB.Scopes(pagedb).Find(suer)
	conf.DB.Limit(pagesize).Offset(page * pagesize).Find(&hosts)
	total := conf.DB.Find(&[]sql.Host{}).RowsAffected

	var resultHosts []ResultHost

	for _, host := range hosts {
		var accounts []sql.Account
		conf.DB.Where("host_id=?", strconv.Itoa(int(host.ID))).Find(&accounts)
		resultHosts = append(resultHosts, ResultHost{
			host,
			accounts,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"page":     page,
		"pagesize": pagesize,
		"total":    total,
		"hosts":    resultHosts,
	})

}

func getDev(c *gin.Context) {
	id := c.Param("id") //取出参数中的id
	_, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "请检查id是否是数字",
		})
		return
	}
	//查找数据库中符合条件的用户
	host := &sql.Host{}
	len := conf.DB.Where("id=?", id).First(host).RowsAffected
	if len == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"host": nil,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"host": host,
	})

}

func postDev(c *gin.Context) {

	host := &sql.Host{}

	err := c.BindJSON(host)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "请求参数解析失败，请检查参数是否有误",
		})
		return
	}

	fmt.Printf("%#v", host)

	result := conf.DB.Create(host)
	if result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": result.Error.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"msg":  "创建资产成功",
		"user": host,
	})
}

func putDev(c *gin.Context) {

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "请检查id是否是数字",
		})
		return
	}
	//查找数据库中符合条件的用户
	host := &sql.Host{}
	err = c.BindJSON(host)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "请求参数解析失败，请检查参数是否有误",
		})
		return
	}
	host.ID = uint(id)

	conf.DB.Save(host)
	c.JSON(http.StatusOK, gin.H{
		"msg": "更新成功",
	})
}

func deleteDev(c *gin.Context) {

	id := c.Param("id")
	_, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "请检查id是否是数字",
		})
		return
	}
	host := &sql.Host{}
	len := conf.DB.Where("id=?", id).First(host).RowsAffected
	if len != 1 {
		c.JSON(http.StatusNotFound, gin.H{
			"msg": "没有找到id " + id + " 的资产",
		})
		return
	}

	result := conf.DB.Delete(host)

	if result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": result.Error.Error(),
		})
		return
	}

	c.JSON(http.StatusNoContent, gin.H{
		"msg":  "删除成功",
		"user": host,
	})
}
