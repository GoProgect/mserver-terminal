package route

import (
	"more_ssh/middleware"
	"more_ssh/myssh"
	"more_ssh/util"

	"github.com/gin-gonic/gin"
)

func Init(r *gin.Engine) {
	r.GET("/myssh", myssh.RunWebSSH)
	r.Use(util.Cors())      //解决跨域问题
	r.POST("/login", login) //登录
	//审计路由
	replayer := r.Group("/replayer")
	{
		replayer.GET("tui/:id", replayerTui) //字符回放
	}

	api := r.Group("/api")
	api.Use(middleware.ParseToken)
	{
		//用户增删改查
		api.GET("/user/list", userlist)
		api.GET("/user/:id", getUser)
		api.POST("/user", postUser)
		api.PUT("/user/:id", putUser)
		api.DELETE("/user/:id", deleteUser)

		// 资产的增删改查
		api.GET("/dev/list", list)
		api.GET("/dev/:id", getDev)
		api.POST("/dev", postDev)
		api.PUT("/dev/:id", putDev)
		api.DELETE("/dev/:id", deleteDev)

		// 托管账号的增删改查
		api.GET("/account/:id", listAccount)
		api.POST("/account", postAccount)
		api.PUT("/account", putAccount)
		api.DELETE("/account/:id", deleteAccount)

		//查询审计
		api.GET("/audit/list", auditlist)

	}

}
