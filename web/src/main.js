import { createApp } from 'vue'
import App from './App.vue'
import naive from './plugins/naive'
import 'vfonts/Lato.css'
import 'vfonts/FiraCode.css'
import axios from './net/axios'
import router from './router'
import 'virtual:svg-icons-register'

const app = createApp(App)

app.use(naive)
app.use(router)
app.config.globalProperties.$http = axios
app.mount('#app')
