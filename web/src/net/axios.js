import axios from 'axios'
// axios.defaults.baseURL = 'http://lyxkj.8866.org:7180/api/private/v1/'
axios.defaults.baseURL = 'http://127.0.0.1:8080/'
axios.interceptors.request.use((config) => {
  config.headers.Authorization = window.sessionStorage.getItem('Authorization')
  return config
})

export default axios
