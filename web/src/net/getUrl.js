/**
 * 获取get请求提交的参数
 * url是请求完整的连接
 * key是键值对的键值
 *
 * 返回key对应的value值
 */
export default (url, key) => {
  var query = url.search.substring(1)
  var keys = query.split('&')
  for (var i = 0; i < keys.length; i++) {
    var pair = keys[i].split('=')
    if (pair[0] == key) {
      return pair[1]
    }
  }
  return false
}
