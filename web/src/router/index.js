import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/login', component: () => import('../views/login/Login.vue') },
    {
      path: '/webssh',
      component: () => import('../views/resourceaccess/Webssh.vue'),
    },
    {
      path: '/replayer/tui',
      component: () => import('../views/audit/tui/RepalyTui.vue'),
    },
    {
      path: '/home',
      component: () => import('../views/home/Home.vue'),
      children: [
        // 工作台
        {
          path: 'dashboard',
          component: () => import('../views/dashboard/Dashboard.vue'),
        },
        //访问资产
        {
          path: 'resourceaccess',
          component: () => import('../views/resourceaccess/Resourceaccess.vue'),
        },
        // 用户
        {
          path: 'userlist',
          component: () => import('../views/user/UserList.vue'),
        },
        {
          path: 'userrole',
          component: () => import('../views/user/UserRole.vue'),
        },

        // 资产
        {
          path: 'host',
          component: () => import('../views/dev/Host.vue'),
        },
        {
          path: 'network',
          component: () => import('../views/dev/Network.vue'),
        },
        {
          path: 'service',
          component: () => import('../views/dev/Service.vue'),
        },
        // 账号
        {
          path: 'changepwd',
          component: () => import('../views/account/ChangePasswd.vue'),
        },
        {
          path: 'pwdrole',
          component: () => import('../views/account/PasswdRole.vue'),
        },
        {
          path: 'pwdcheck',
          component: () => import('../views/account/PasswdCheck.vue'),
        },
        //审计
        {
          path: 'tui',
          component: () => import('../views/audit/tui/TuiLog.vue'),
        },
        {
          path: 'online',
          component: () => import('../views/audit/Online.vue'),
        },
        {
          path: 'gui',
          component: () => import('../views/audit/gui/GuiLog.vue'),
        },
        {
          path: 'tuireplayer',
          component: () => import('../views/audit/tui/RepalyTui.vue'),
        },

        //系统
        {
          path: 'basicsystem',
          component: () => import('../views/system/BasicSeting.vue'),
        },
        {
          path: 'license',
          component: () => import('../views/system/License.vue'),
        },
        {
          path: 'systemstat',
          component: () => import('../views/system/SystemStat.vue'),
        },
      ],
    },
    { path: '/', redirect: '/home' },
  ],
})

//前置导航守卫
router.beforeEach((to, from, next) => {
  // console.log(to,from)
  if (to.fullPath === '/login') return next()

  if (!window.sessionStorage.getItem('Authorization')) return next('/login')

  next()
})

export default router
