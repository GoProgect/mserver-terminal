/**
 * 格式化时间戳
 * 传入一个时间单位是s的时间戳
 */

const formateData = {}

//传入一个秒级单位时间戳，返回一个 yyyy-mm-dd hh:mm:ss 格式的string
formateData.formateDate = function (Timestamp) {
  let time = new Date(Timestamp * 1000)
  return (
    time.getFullYear() +
    '-' +
    (time.getMonth() + 1 >= 10
      ? time.getMonth() + 1
      : '0' + (time.getMonth() + 1)) +
    '-' +
    (time.getDate() >= 10 ? time.getDate() : '0' + time.getDate()) +
    ' ' +
    (time.getHours() >= 10 ? time.getHours() : '0' + time.getHours()) +
    ':' +
    (time.getMinutes() >= 10 ? time.getMinutes() : '0' + time.getMinutes()) +
    ':' +
    (time.getSeconds() >= 10 ? time.getSeconds() : '0' + time.getSeconds())
  )
}

//传入Timestamp 秒级单位时间戳，返回一个hh:mm:ss 格式的string
formateData.formateTime = function (Timestamp) {
  let h, m, s, tmp

  if (Timestamp > 3600) {
    h = parseInt(Timestamp / 3600)
    if (h < 10) {
      h = '0' + h
    }
    tmp = Timestamp % 3600
  } else {
    h = '00'
    tmp = Timestamp
  }

  if (tmp > 60) {
    m = parseInt(tmp / 60)
    if (m < 10) {
      m = '0' + m
    }
    tmp = tmp % 60
  } else {
    m = '00'
  }

  if (tmp < 10) {
    s = '0' + tmp
  } else {
    s = tmp
  }

  return h + ':' + m + ':' + s
}

export default formateData
