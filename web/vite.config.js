import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import viteSvgIcons from 'vite-plugin-svg-icons'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    viteSvgIcons({
      // Specify the icon folder to be cached
      iconDirs: [path.resolve(process.cwd(), 'src/icon/svg/')],
      // Specify symbolId format
      symbolId: 'icon-[name]',
    }),
  ],
  // server: {
  //   proxy: {
  //     '/more': {
  //       target: 'http://lyxkj.8866.org:7180/api/private/v1/',
  //       changeOrigin: true,
  //     },
  //   },
  // },
})
