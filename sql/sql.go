package sql

import (
	"gorm.io/gorm"
)

// 定义一个用户的表
type User struct {
	ID        uint           `gorm:"primarykey" json:"id"`
	User      string         `gorm:"not null;unique" json:"user"`
	Name      string         `gorm:"not null" json:"name"`
	Email     string         `json:"email"`
	Phone     string         `json:"phone"`
	Status    bool           `gorm:"integer;not null" json:"status"`
	Role      uint8          `gorm:"integer;not null" json:"role"`
	Password  string         `gorm:"not null" json:"password"`
	CreatedAt int            `json:"createdAt"`
	UpdatedAt int            `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deletedAt" `
}

/**
* Type:     1-Linux 2-windows 3-NetWork
* Service:  ssh     rdp       telnet     vnc
 */
type Host struct {
	ID          uint           `gorm:"primarykey" json:"id"`
	Name        string         `gorm:"not null;unique" json:"name"`
	IP          string         `gorm:"not null" json:"ip"`
	Status      bool           `gorm:"integer;not null" json:"status"`
	Service     string         `gorm:"integer;not null" json:"service"`
	Port        uint           `gorm:"integer;not null" json:"port"`
	Type        uint           `gorm:"integer;not null" json:"type"`
	Account     string         `json:"account"`
	Password    string         `json:"password"`
	Description string         `json:"description"`
	CreatedAt   int            `json:"createdAt"`
	UpdatedAt   int            `json:"updatedAt"`
	DeletedAt   gorm.DeletedAt `gorm:"index" json:"deletedAt" `
}

type Account struct {
	ID        uint           `gorm:"primarykey" json:"id"`
	Account   string         `gorm:"string;not null" json:"account"`
	Password  string         `json:"password"`
	HostId    uint           `gorm:"not null" json:"hostId"`
	Status    bool           `json:"status"`
	IsNull    bool           `json:"isNull"`
	CreatedAt int            `json:"createdAt"`
	UpdatedAt int            `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deletedAt" `
}

type Audit struct {
	ID        uint   `gorm:"primarykey" json:"id"`
	Account   string `gorm:"string;not null" json:"account"`
	StartTime int    `json:"startTime"`
	EndTime   int    `json:"endTime"`
	FilePath  string `json:"filePath"`
	ClientIp  string `json:"clientIp"`
	UserId    uint   `json:"userId"`
	HostId    uint   `json:"hostId"`
	Service   string `json:"service"`
}
