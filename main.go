package main

import (
	"fmt"
	"more_ssh/route"
	"more_ssh/sql"

	"github.com/gin-gonic/gin"
)

func main() {

	// 初始化数据库
	err := sql.Init()
	if err != nil {
		fmt.Println("数据库读取失败")
		return
	}

	r := gin.Default()

	route.Init(r)
	r.Run()

}
